       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MONTHINC.
       AUTHOR. kittipon thaweelarb.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-PROV-INPUT ASSIGN TO "PROV.DAT"
              ORGANIZATION IS SEQUENTIAL
              FILE  STATUS IS WS-PROV-INPUT-STATUS.
           SELECT 200-MONTH-INPUT ASSIGN TO "MONTHR.DAT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE  STATUS IS WS-MONTH-INPUT-STATUS.
              SELECT 300-MONTH-OUTPUT ASSIGN TO "INCOME.RPT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE  STATUS IS WS-MONTH-OUTPUT-STATUS.

       DATA DIVISION. 
       FILE SECTION. 
       FD 100-PROV-INPUT
             BLOCK CONTAINS 0 RECORDS.
       01 PROV-INPUT-REOCRD.
          05 PRO-CODE              PIC X(3).
          05 PRO-NAME              PIC X(20).
          05 FILLER                PIC X(7).
       FD 200-MONTH-INPUT
           BLOCK CONTAINS 0 RECORDS.
       01 MONTH-INPUT-REOCORD.
          05 MONTH-NUM             PIC X(2).
          05 FILLER                PIC X(5).
          05 MONTH-PRO-CODE        PIC X(3).
          05 FILLER                PIC X(7).
          05 MONTH-INCOME          PIC X(12).
       FD 300-MONTH-OUTPUT
           BLOCK CONTAINS 0 RECORDS.
       01 INCOME-OUTPUT-RECORD     PIC X(80).



       WORKING-STORAGE SECTION. 
       01 WS-PROV-INPUT-STATUS     PIC X(2).
          88 FILE-OK                                   VALUE "00".
          88 FILE-AT-END                               VALUE "10".
       01 WS-MONTH-INPUT-STATUS    PIC X(2).
          88 FILE-OK                                   VALUE "00".
          88 FILE-AT-END                               VALUE "10".
       01 WS-MONTH-OUTPUT-STATUS   PIC X(2).
          88 FILE-OK                                   VALUE "00".
          88 FILE-AT-END                               VALUE "10".
       01 WS-CACULATION.
          05 WS-PROV-INPUT-COUNT   PIC 9(5)            VALUE ZEROS.
          05 WS-MONTH-INPUT-COUNT  PIC 9(5)            VALUE ZEROS.
      *   05 WS-INDEX-PROV        PIC 9(5)  VALUE 1.
          05 WS-PROVINCE OCCURS 6 TIMES INDEXED BY IDX-PROV.
             10 WS-PRO-CODE        PIC X(3).
             10 WS-PRO-NAME        PIC X(20).
             10 WS-INCOME-TOTAL    PIC 9(9)V99         VALUE ZEROES.
          05 WS-MONTH.
             10 WS-MONTH-NUM       PIC X(2).
             10 WS-MONTH-PRO-CODE  PIC X(3).
             10 WS-MONTH-INCOME    PIC 9(7)V99.
       01 PRT-REPORT-FORMAT.
          05 RPT-HEADER            PIC X(38)           VALUE
                "CODE  NAME                TOTAL INCOME".
          05 RPT-DETAIL.
             10 RPT-PRO-CODE       PIC X(3).
             10 FILLER             PIC X(2)            VALUE SPACES.
             10 RPT-PRO-NAME       PIC X(20).
             10 RPT-INCOME-TOTAL   PIC $$$$,$$$,$$9.99.
             
       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU 1000-EXIT.
           PERFORM 2000-PROCESS THRU 2000-EXIT.
           PERFORM 3000-END THRU 3000-EXIT. 
           GOBACK.

       1000-INITIAL.
           PERFORM 1100-OPEN-FILE THRU 1100-EXIT
           PERFORM 8100-READ-PROV THRU 8100-EXIT

           PERFORM 1200-OPEN-FILE THRU 1200-EXIT 
           PERFORM 8200-READ-MONTH THRU 8200-EXIT

           PERFORM 1300-OPEN-FILE THRU 1300-EXIT 
           .
       1000-EXIT.
           EXIT.
       1100-OPEN-FILE.
           OPEN INPUT 100-PROV-INPUT
           IF FILE-OK OF WS-PROV-INPUT-STATUS
              CONTINUE
           ELSE
              DISPLAY "***** HONTHINC ABEND *****"
                 UPON CONSOLE  
              DISPLAY "***** PARA 1000-INITIAL FAIL *****"
                 UPON CONSOLE 
              DISPLAY "***** FILE STATUS :" WS-PROV-INPUT-STATUS "*****"
                 UPON CONSOLE 
              DISPLAY "***** HONTHINC ABEND *****"
                 UPON CONSOLE 
           END-IF
           .
       1100-EXIT.
           EXIT.
       1200-OPEN-FILE.
           OPEN INPUT 200-MONTH-INPUT 
           IF FILE-OK OF WS-MONTH-INPUT-STATUS
              CONTINUE
           ELSE
              DISPLAY "***** HONTHINC ABEND *****"
                 UPON CONSOLE  
              DISPLAY "***** PARA 1200-OPEN-FILE FAIL *****"
                 UPON CONSOLE 
              DISPLAY "***** FILE STATUS :"
                      WS-MONTH-INPUT-STATUS
                      "*****"
                 UPON CONSOLE 
              DISPLAY "***** HONTHINC ABEND *****"
                 UPON CONSOLE 
           END-IF
           .
       1200-EXIT.
           EXIT.
       1300-OPEN-FILE.
           OPEN OUTPUT 300-MONTH-OUTPUT 
           IF FILE-OK OF WS-MONTH-OUTPUT-STATUS
              CONTINUE
           ELSE
              DISPLAY "***** HONTHINC ABEND *****"
                 UPON CONSOLE  
              DISPLAY "***** PARA 1300-OPEN-FILE FAIL *****"
                 UPON CONSOLE 
              DISPLAY "***** FILE STATUS :"
                      WS-MONTH-OUTPUT-STATUS
                      "*****"
                 UPON CONSOLE 
              DISPLAY "***** HONTHINC ABEND *****"
                 UPON CONSOLE 
           END-IF
           .
       1300-EXIT.
           EXIT.
       2000-PROCESS.
      *    DISPLAY PRO-CODE " " PRO-NAME 

      *    MOVE PRO-CODE TO WS-PRO-CODE(WS-INDEX-PROV)
      *    MOVE PRO-NAME TO WS-PRO-NAME(WS-INDEX-PROV)
      *    ADD 1 TO WS-INDEX-PROV 
      *    PERFORM 8100-READ-PROV THRU 8100-EXIT

           PERFORM 4000-LOAD THRU 4000-EXIT
              UNTIL FILE-AT-END OF WS-PROV-INPUT-STATUS
           PERFORM 2100-PROCESS-MONTH THRU 2100-EXIT
              UNTIL FILE-AT-END OF WS-MONTH-INPUT-STATUS 
           .
       2000-EXIT.
           EXIT.
       2100-PROCESS-MONTH.
      *    DISPLAY MONTH-INPUT-REOCORD 
      *    DISPLAY MONTH-NUM " " MONTH-PRO-CODE " " MONTH-INCOME 
           MOVE MONTH-NUM TO WS-MONTH-NUM 
           MOVE MONTH-PRO-CODE TO WS-MONTH-PRO-CODE 
           MOVE MONTH-INCOME TO WS-MONTH-INCOME 
      *    DISPLAY WS-MONTH-NUM
      *            " "
      *            WS-MONTH-PRO-CODE
      *            " "
      *            WS-MONTH-INCOME 
           PERFORM VARYING IDX-PROV FROM 1 BY 1 UNTIL IDX-PROV > 6
                   IF WS-MONTH-PRO-CODE = WS-PRO-CODE(IDX-PROV) 
                      ADD WS-MONTH-INCOME
                         TO WS-INCOME-TOTAL(IDX-PROV) 
                      EXIT PERFORM
                   END-IF
           END-PERFORM.
           PERFORM 8200-READ-MONTH THRU 8200-EXIT
           .
       2100-EXIT.
           EXIT.

       3000-END.
           DISPLAY RPT-HEADER.
           MOVE RPT-HEADER TO INCOME-OUTPUT-RECORD 
           PERFORM 7000-WRITE THRU 7000-EXIT
           PERFORM VARYING IDX-PROV
              FROM 1 BY 1 UNTIL IDX-PROV > 6
      *            DISPLAY WS-PRO-CODE(IDX-PROV)
      *                    " "
      *                    WS-PRO-NAME(IDX-PROV)
      *                    " "
      *                    WS-INCOME-TOTAL(IDX-PROV)
                   MOVE WS-PRO-CODE(IDX-PROV) TO RPT-PRO-CODE 
                   MOVE WS-PRO-NAME(IDX-PROV) TO RPT-PRO-NAME 
                   MOVE WS-INCOME-TOTAL(IDX-PROV) TO RPT-INCOME-TOTAL 
                   DISPLAY RPT-DETAIL 
                   MOVE RPT-DETAIL TO INCOME-OUTPUT-RECORD 
                   PERFORM 7000-WRITE THRU 7000-EXIT
           END-PERFORM.
           DISPLAY "Read 100-PROV-INPUT-COUNT "
                   WS-PROV-INPUT-COUNT
                   " RECORDS."
           DISPLAY "Read 200-MONTH-INPUT "
                   WS-MONTH-INPUT-COUNT
                   " RECORDS."
           CLOSE 100-PROV-INPUT 200-MONTH-INPUT 300-MONTH-OUTPUT 
           .
       3000-EXIT.
           EXIT.

       4000-LOAD.
           MOVE PRO-CODE TO WS-PRO-CODE(IDX-PROV)
           MOVE PRO-NAME TO WS-PRO-NAME(IDX-PROV)
           ADD 1 TO IDX-PROV 
           PERFORM 8100-READ-PROV THRU 8100-EXIT
           .
       4000-EXIT.
           EXIT.

       7000-WRITE.
           WRITE INCOME-OUTPUT-RECORD
           IF FILE-OK OF WS-MONTH-OUTPUT-STATUS  
              CONTINUE
           ELSE
              IF FILE-AT-END OF WS-MONTH-INPUT-STATUS
                 DISPLAY "***** HONTHINC ABEND *****"
                    UPON CONSOLE  
                 DISPLAY "***** PARA 7000-WRITE FAIL *****"
                    UPON CONSOLE 
                 DISPLAY "***** FILE STATUS :"
                         WS-MONTH-OUTPUT-STATUS
                         "*****"
                    UPON CONSOLE 
                 DISPLAY "***** HONTHINC ABEND *****"
                    UPON CONSOLE 
              END-IF 
           .
       7000-EXIT.
           EXIT.
       
       8100-READ-PROV.
           READ 100-PROV-INPUT
           IF FILE-OK OF WS-PROV-INPUT-STATUS 
              ADD 1 TO WS-PROV-INPUT-COUNT 
           ELSE
              IF FILE-AT-END OF WS-PROV-INPUT-STATUS
                 CONTINUE
              ELSE
                 DISPLAY "***** HONTHINC ABEND *****"
                    UPON CONSOLE  
                 DISPLAY "***** PARA 8100-READ-PROV FAIL *****"
                    UPON CONSOLE 
                 DISPLAY "***** FILE STATUS :"
                         WS-PROV-INPUT-STATUS
                         "*****"
                    UPON CONSOLE 
                 DISPLAY "***** HONTHINC ABEND *****"
                    UPON CONSOLE 
              END-IF 
           END-IF 
           .
       8100-EXIT.
           EXIT.

       8200-READ-MONTH.
           READ 200-MONTH-INPUT
           IF FILE-OK OF WS-MONTH-INPUT-STATUS 
              ADD 1 TO WS-MONTH-INPUT-COUNT 
           ELSE
              IF FILE-AT-END OF WS-MONTH-INPUT-STATUS
                 CONTINUE
              ELSE
                 DISPLAY "***** HONTHINC ABEND *****"
                    UPON CONSOLE  
                 DISPLAY "***** PARA 8200-READ-MONTH FAIL *****"
                    UPON CONSOLE 
                 DISPLAY "***** FILE STATUS :"
                         WS-MONTH-INPUT-STATUS
                         "*****"
                    UPON CONSOLE 
                 DISPLAY "***** HONTHINC ABEND *****"
                    UPON CONSOLE 
              END-IF 
           END-IF 
           .
       8200-EXIT.
           EXIT.